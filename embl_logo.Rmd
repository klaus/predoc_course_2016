---
title: "EMBL Logo"
output:
  word_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Data

ftp://ftp-exchange.embl.de/pub/exchange/msmith/outgoing/

http://tiny.cc/fcbcgy


# Functions you may need

`load()`, `ls()`, `library()`, `ggplot()`, `geom_point()`

# Examples

```{r, eval=TRUE, echo=TRUE, fig.width=10, fig.height=10, fig.align='center'}
library(ggplot2)
library(gridExtra)
load("hex_grid.Rdata")
#load("~/Teaching/predoc_course_2016/hex_grid.Rdata")

point_size <- 10

#hex_grid$layer <- factor(hex_grid$layer, 
#                         levels = sort(unique(hex_grid$layer)) )

p1 <- ggplot(hex_grid, aes(x = x, y = y)) + 
    geom_point() 

p2 <- p1 + 
    geom_point(aes(color = layer, size = layer)) +
    scale_size(trans = "reverse")

p3 <- p1 + 
    geom_point(size = point_size , aes(color = lab)) +
    coord_fixed(5) 

p4 <- ggplot(hex_grid, aes(x = x, y = y)) + 
    geom_point(size = point_size, aes(color = lab)) + 
    coord_fixed() +
    scale_color_manual(values=c("#E2001A", "#6FAA46")) +
    theme_bw() +
    theme(panel.border = element_blank(), 
          panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(), 
      axis.text = element_blank(),
      line = element_blank(),
      title = element_blank())

grid.arrange(p1, p2, p3, p4, ncol = 2)
```

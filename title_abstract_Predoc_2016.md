Visual Exploration and Handling of Biological Data
----------------------------------------------------

Data visualization enables a scientist to effectively explore data and make 
discoveries about the complex processes at work. In this practical, we will 
perform visual exploratory analysis on various small and large scale data sets
using R and the ggplot2 R-package. 

We will furthermore discuss data handling using the split--apply--combine paradigm
and introduce the concept of tidy data. It will be shown how to turn any kind 
of data into a "tidy" format that then allows for a straightforward downstream analysis.

Visual exploration and data handling will finally be illustrated on real--world case
studies. This practical will teach basic computational tools useful for anyone dealing 
with biological data. 



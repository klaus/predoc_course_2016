---
title: "EMBL Logo"
output:
  output: html_notebook
---
```{r}
library(dplyr)

```


# create points on the perimeter of a hexagon

```{r}
calcHexagonVertices <- function(size = 1) {

    if(size == 0) {
        x <- 0
        y <- 0
    } else {

        x <- c(0,
               0,
               rep(size * sqrt(3)/2, size+1),
               -rep(size * sqrt(3)/2, size+1),
               rep((size-1):1 * sqrt(3)/2, times = 2),
               -rep((size-1):1 * sqrt(3)/2, times = 2))

        y <- c(size,
               -size,
               seq((size/2), -size/2, by = -1),
               seq((size/2), -size/2, by = -1),
               seq(size/2, size, length.out = size+1)[2:(size)],
               -seq(size/2, size, length.out = size+1)[2:(size)],
               seq(size/2, size, length.out = size+1)[2:(size)],
               -seq(size/2, size, length.out = size+1)[2:(size)])
    }

    points <- data.frame(x = x, y = y, layer = size)
    return(points)
}
```



# now create all the layers

```{r}
concentricHexagon <- function(size = 4) {
    res <- lapply(0:size, FUN = calcHexagonVertices)
    res <- do.call('rbind', res)
    return(res)
}


hex_grid <- concentricHexagon()
hex_grid <- cbind(hex_grid, lab = c(rep('Other Lab', 22), 'EMBL', rep('Other Lab', 46)))

```

